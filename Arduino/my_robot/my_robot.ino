#include <SoftwareSerial.h>  

// ---------------------------------------------------------------
// DEFINE
// ---------------------------------------------------------------

// pinos de dados do bluetooth
#define rxPin 0
#define txPin 1

// ---------------------------------------------------------------
// CONSTANTE
// ---------------------------------------------------------------

const String charStartMessage = "s";
const String charEndMessage = "e";
const char endChar = 'e';
const String charLeft = "l";
const String charRight = "r";
const String charAhead = "a";

// ---------------------------------------------------------------
// VARIABLES
// ---------------------------------------------------------------

// variavel utilizadas pelo bluetooth.
String inputString = "";         // a string que conterah a mensagem recebida
boolean receivedControl = false;  // boolean de controle para quando a mensagem estiver completa

// Biblioteca que define a variavel serial para o uso do bluetooth
SoftwareSerial bluetoothSerial(rxPin, txPin); // RX, TX   


// pino 2 da ponte H L293D
int motorLeft1  = 7;
// pino 7 da ponte H L293D
int motorLeft2  = 6;
// pino 14 da ponte H L293D
int motorRight1 = 9;
// pino 10 da ponte H L293D
int motorRight2 = 8;

int enableLeft = 11;
int enableRight = 10;

// ---------------------------------------------------------------
// DECLARATION OF METHODS
// ---------------------------------------------------------------
          
void travaMotor();  
void destravaMotor(int vel, int direction);

// funcao que seta os dados do bluetooth
void setupBluetooth();
// funcao que le os dados do bluetooth
void readSerialBluetooth();
// funcao que envia dados via bluetooth
void sendMessageWithBluetooth(int option);
// funcao que processa os dados recebido e controla o robo
void dataManager();


// ---------------------------------------------------------------
// SETUP ARDUINO
// ---------------------------------------------------------------
   
void setup() {
   //define pinos dos motores
   pinMode(motorLeft1, OUTPUT);
   pinMode(motorLeft2, OUTPUT);
   pinMode(motorRight1, OUTPUT);
   pinMode(motorRight2, OUTPUT);

   pinMode(enableLeft, OUTPUT);
   pinMode(enableRight, OUTPUT);
 
	   
  setupBluetooth();

}//end setup

// ---------------------------------------------------------------
// LOOP ARDUINO
// ---------------------------------------------------------------
   
void loop() {
  readSerialBluetooth();
  
  String test = receivedControl ? "TRUE" : "FALSE";
  Serial.println("teste: " + test);
  if(receivedControl) {
     
    dataManager();
    sendMessageWithBluetooth(0);

    travaMotor();
   }
   
}// END loop()

// ---------------------------------------------------------------
// MY METHODS
// ---------------------------------------------------------------

void travaMotor() {
  analogWrite(enableLeft, 0);
  analogWrite(enableRight, 0);
  
    digitalWrite(motorLeft1 , LOW);
    digitalWrite(motorLeft2 , LOW);
    digitalWrite(motorRight1 , LOW);
    digitalWrite(motorRight2 , LOW);
}
  
/**
 * int vel -> velocidade de 0-255
 * bool ahead -> TRUE == ahead. FALSE == reverse
 * int turn -> '-1'  == turn left. '0' == not turn. '1' == turn right. 
*/
void destravaMotor(int vel, bool ahead, int turn) {

// not turn
if (turn == 0) {
	// just ahead
	analogWrite(enableLeft, vel);
	analogWrite(enableRight, vel);
}
// turn left
else if (turn == -1) {
	analogWrite(enableLeft, 255);
	analogWrite(enableRight, vel);

}
// turn right
else if (turn == 1) {
	analogWrite(enableLeft, vel);
	analogWrite(enableRight, 255);

}

// config motor ahead
if (ahead) {
	digitalWrite(motorLeft1 , HIGH);
	digitalWrite(motorLeft2 , LOW);
	digitalWrite(motorRight1 , HIGH);
	digitalWrite(motorRight2 , LOW);
}
// config motor reverse
else {
	digitalWrite(motorLeft1 , LOW);
	digitalWrite(motorLeft2 , HIGH);
	digitalWrite(motorRight1 , LOW);
	digitalWrite(motorRight2 , HIGH);
}

  
  /*    
    //mostra no log os valores de interios lidos no array
				Serial.println("Escrito");
				Serial.print(L1);
				Serial.print(" ");
				Serial.print(L2);
				Serial.print(" ");
				Serial.print(R1);
        Serial.print(" ");
				Serial.println(R2);
       */
}

/**
 * Essa funcao envia os dados via bluetooth. Essa funcao recebe como parametro uma das opcoes:
 * 1 - MENSAGEM DE OK - para quando o robo chegou no fim percurso
 * 2 - MENSAGEM DE INCOMPLETO - para quando o robo terminou de executar os comandos e nao chegou no objetivo
 * 3 - MENSAGEM DE COLISAO - para quando o robo colidir
 */
void sendMessageWithBluetooth(int option) {

  if (option == 1) {
    bluetoothSerial.println("ARRIVED");
  }
  else if (option == 2) {
    bluetoothSerial.println("INCOMPLETE");
  }
  else if (option == 3) {
    bluetoothSerial.println("CHASH");
  }
  else
    bluetoothSerial.println("Deu treta");

    // reseta variaveis de controle
    receivedControl = false;
    inputString = "";
    
}

/**
 * Processa a string recebida. Para cada caracter recebido eh chamado o controle do motor.
 */
void dataManager() {
  /**
   * a string recebida devera ser da seguinte forma "s0000e"
   * onde as posicoes representam
   * 0 - 's' caracter que representa o inicio da mensagem
   * 1 - [0-1] 1 representa aceleracao para frente. 0 representa que esta inativo
   * 2 - [0-1] 1 representa aceleracao para tras. 0 representa que esta inativo
   * 3 - [0-9] se maior que 0 representa que esta fazendo a curva para esquerda
   * 4 - [0-9] se maior que 0 representa que esta fazendo a curva para direita 
   * 5 - 'e' caracter que representa o fim da mensagem
   */
   //Serial.println("Recebeu: " + inputString + ", size: " + inputString.length());
   
   String temp = "";
   
   // TODO o erro começa aqui a string recebi pode ser maior que 6 caracteres
   
   for(int i = 0; i < inputString.length(); i++) {
     if(inputString.substring(i) == "s") 
       temp = inputString.substring(i, i+5);
     }
     Serial.println("Recebeu: " + inputString + ", converteu: " + temp + ", size: " + temp.length());
   
    if (inputString.length() == 6) {
      
   
      // verifica se primeiro e ultimo caracter eh marcador
    	if (inputString.substring(0) == charStartMessage && inputString.substring(5) == charEndMessage) {
		    // recupera valores direita, esquerda, frente e tras
		    int right, left;
		    int ahead, reverse;
		    
		    ahead = inputString.substring(1).toInt();
		    reverse = inputString.substring(2).toInt();
		    left = inputString.substring(3).toInt();
		    right = inputString.substring(4).toInt();

			/**
			 * 0 - ahead
			 * 1 - right
			 * 2 - left
			 * 3 - reverse
			 */

			// esta indo para frente ou para tras sem virar
			if (right == 0 && left == 0) {
				if (ahead == 1 && reverse == 0)
					destravaMotor(255, true, 0);
				else if(ahead == 0 && reverse == 1)
					destravaMotor(255, false, 0);
				else
					travaMotor();
			}
			// esta virando
			else {
				// esta para frente
				if (ahead == 1 && reverse == 0) {
					// esta virando para direita
					if (left == 0 && right > 0) {
						int vel = 255 - (right * 255 / 9);
						destravaMotor(vel, true, 1);
					}
					// esta virando para esquera
					else if (left > 0 && right == -1) {
						int vel = 255 - (left * 255 / 9);
						destravaMotor(vel, true, 0);
					}
				}
			
				else if(ahead == 0 && reverse == 1) {
					// esta virando para direita
					if (left == 0 && right > 0) {
						int vel = 255 - (right * 255 / 9);
						destravaMotor(vel, false, 1);
					}
					// esta virando para esquera
					else if (left > 0 && right == 0) {
						int vel = 255 - (left * 255 / 9);
						destravaMotor(vel, false, -1);
					}
				}
		  	}  
		}
    }
}

/**
 * O setupBluetooth deve ser chamado no setup do projeto. Essa funcao aloca o tamanho da string, e se preenche as mensagem de saldacao caso a conexao seja estabelicida
 * no log do arduino aparece 'oioioi'
 * no received do android aparece 'Hello from Arduino'
 */
void setupBluetooth() {
  // aloca 200 bytes para a string
  inputString.reserve(200);
  Serial.begin(9600);
  bluetoothSerial.begin(9600);
  //bluetoothSerial.println("Hello from Arduino");
  //Serial.println("oioioi");  
}

/**
 * Essa funcao recebe os dados do bluetooth e converte os char recebido em uma string a cada loop do projeto ele intera mais um caracter.
 * Quando o caracter terminal eh encontrado seta o boolean de controle como TRUE, com isso eh garantido que toda a mensagem foi recebida.
 */
void readSerialBluetooth() {
  if (bluetoothSerial.available()) {
     
   char inChar = (char) bluetoothSerial.read();
    inputString += inChar;
   
   // procura caracter terminal, nesse caso 'e'
    if (inChar == endChar) {
      receivedControl = true;
    }
  }
}
  
  
