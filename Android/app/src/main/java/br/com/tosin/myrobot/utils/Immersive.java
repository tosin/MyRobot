package br.com.tosin.myrobot.utils;

import android.app.Activity;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import br.com.tosin.myrobot.R;

/**
 * Implementação do Immersive Android, com isso a actionBar ou toolbar é escondida apos um tempo pre determinado;
 * É necessário chamar o método onWindowFocusChanged(boolean hasFocus), na activity que esta usando o immersive;
 * Created by tosin on 31/03/16.
 */
public class Immersive {
    private Activity activity;

    private View mDecorView;
    private static int INITIAL_HIDE_DELAY = 300;

    /**
     * Seta a activity que usará o immersive;
     * @param activity
     */
    public Immersive (Activity activity, int idLayout) {
        this.activity = activity;

        mDecorView = activity.getWindow().getDecorView();
        confFullScreen(idLayout);
    }

    /**
     * Seta a activity que usara o immersive, seta um tempo que o toolbar irá permacer na tela;
     * Delay default == 300;
     * @param activity
     * @param delay
     */
    public Immersive (Activity activity, int idLayout, int delay) {
        this.activity = activity;
        INITIAL_HIDE_DELAY = delay != 0 ? delay : 300;

        mDecorView = activity.getWindow().getDecorView();
        confFullScreen(idLayout);
    }


    // ----------------------------------------------------------------------------------------------
    // METHODS PUBLIC
    // ----------------------------------------------------------------------------------------------

    public void onWindowFocusChanged(boolean hasFocus) {

        // When the window loses focus (e.g. the action overflow is shown),
        // cancel any pending hide action. When the window gains focus,
        // hide the system UI.
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            if (hasFocus) {
                delayedHide(INITIAL_HIDE_DELAY);
            } else {
                mHideHandler.removeMessages(0);
            }
        }
    }

    private void confFullScreen(int idLayout){
        final View controlsView = activity.findViewById(idLayout);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            controlsView.setClickable(true);
            final GestureDetector clickDetector = new GestureDetector(activity,
                    new GestureDetector.SimpleOnGestureListener() {
                        @Override
                        public boolean onSingleTapUp (MotionEvent e) {
                            boolean visible = (mDecorView.getSystemUiVisibility()
                                    & View.SYSTEM_UI_FLAG_HIDE_NAVIGATION) == 0;
                            if (visible) {
                                hideSystemUI();
                            } else {
                                showSystemUI();
                            }
                            return true;
                        }
                    });

            controlsView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch (View view, MotionEvent motionEvent) {
                    return clickDetector.onTouchEvent(motionEvent);
                }
            });

            showSystemUI();
        }
    }

    // ----------------------------------------------------------------------------------------------
    // METHODS PRIVATE
    // ----------------------------------------------------------------------------------------------

    // This snippet hides the system bars.
    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        mDecorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    // This snippet shows the system bars. It does this by removing all the flags
    // except for the ones that make the content appear under the system bars.
    private void showSystemUI() {
        mDecorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_VISIBLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        delayedHide(INITIAL_HIDE_DELAY);
    }

    private final Handler mHideHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            hideSystemUI();
        }
    };

    private void delayedHide(int delayMillis) {
        mHideHandler.removeMessages(0);
        mHideHandler.sendEmptyMessageDelayed(0, delayMillis);
    }
}
