package br.com.tosin.myrobot.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.List;

import app.akexorcist.bluetotohspp.library.BluetoothSPP;
import app.akexorcist.bluetotohspp.library.BluetoothState;
import app.akexorcist.bluetotohspp.library.DeviceList;
import br.com.tosin.myrobot.R;
import br.com.tosin.myrobot.ui.activities.MainActivity;

/**
 * Created by tosin on 31/03/16.
 */
public class ManagerBluetooth {

    private static final String TAG = "Bluetooth";
    private BluetoothSPP bt;
//    private Activity activity;

    private ProgressDialog progressDialog;

    public ManagerBluetooth (Activity activity) {
        this.bt = new BluetoothSPP(activity);
//        this.activity = activity;
    }

    /**
     * Verifica se o bluetooth esta conectado, se sim disconectar se não abre a activity para encontrar uma conexão.
     */
    public void connectBluetooth(Activity activity) {
        Message msg = new Message();
        if (bt.getServiceState() == BluetoothState.STATE_CONNECTED) {
            bt.disconnect();
            msg.arg1 = 0;
        } else {
            Intent intent = new Intent(activity, DeviceList.class);
            activity.startActivityForResult(intent, BluetoothState.REQUEST_CONNECT_DEVICE);
        }
    }

    /**
     * Notifica se o bluetooth esta desativado e implementa os listeners de recebimento de dados e de estato da conexão.
     */
    public void initialiseBluetooth (final Activity activity) {

        if (!bt.isBluetoothAvailable()) {

            showSnackBar("Bluetoot não esta ativado", activity);
        }

        bt.setOnDataReceivedListener(new BluetoothSPP.OnDataReceivedListener() {
            public void onDataReceived(byte[] data, String message) {
//                Snackbar.make(activity.getCurrentFocus(), "Dado recebido: " + message, Snackbar.LENGTH_SHORT).show();

                Log.d(TAG, "Bluetooth received: " + message);
                if (progressDialog != null)
                    progressDialog.dismiss();

                Message msg = new Message();
                msg.arg1 = 1;
                msg.obj = message;
                MainActivity.handler.sendMessage(msg);

            }
        });

        bt.setBluetoothConnectionListener(new BluetoothSPP.BluetoothConnectionListener() {
            public void onDeviceConnected (String name, String address) {

                showSnackBar("Conectado à " + name + "\n" + " endereço " + address, activity);

                Message msg = new Message();
                msg.arg1 = 1;
                MainActivity.handler.sendMessage(msg);

                MainActivity.changeFragment();

            }

            public void onDeviceDisconnected() {

                showSnackBar("Conexão perdida", activity);

                bt.disconnect();

                if (progressDialog != null)
                    progressDialog.dismiss();

                Message msg = new Message();
                msg.arg1 = 0;
                MainActivity.handler.sendMessage(msg);

                MainActivity.changeFragment();
            }

            public void onDeviceConnectionFailed() {

                showSnackBar("Incapaz de conectar", activity);

                bt.disconnect();

                if (progressDialog != null)
                    progressDialog.dismiss();

                Message msg = new Message();
                msg.arg1 = 0;
                MainActivity.handler.sendMessage(msg);

                MainActivity.changeFragment();
            }
        });
    }

    private void configLoading(Activity activity) {
        progressDialog = new ProgressDialog(activity);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle("");
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Aguardando a resposta do robô");
    }

    /**
     * Send message
     */
    public void sendMessage(String[] list, Activity activity){


        String information = "";
        information += "s";

        for (String item : list){
            information += item;
        }
        information += "e";

//        Snackbar.make(activity.getCurrentFocus(), "send: " + information, Snackbar.LENGTH_SHORT).show();

        byte[] byteArray = information.getBytes();

        if (bt.getServiceState() == BluetoothState.STATE_CONNECTED){

            configLoading(activity);
//            progressDialog.show();

            bt.send(byteArray, true);
            Log.d(TAG, "Bluetooth sended: " + information);
        } else {
            showSnackBar("Não esta conectado", activity);

        }
    }

    public void start(Activity activity) {
        if (!bt.isBluetoothEnabled()) {
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            activity.startActivityForResult(intent, BluetoothState.REQUEST_ENABLE_BT);
        } else {
            if(!bt.isServiceAvailable()) {
                bt.setupService();
                // AQUI A TAG DEVICE_OTHER DEFINE SE SERÁ CONEXAO ENTRE ANDROID-ANDROID OU ANDROID-OUTRA_COISA
                bt.startService(BluetoothState.DEVICE_OTHER);
//                setup();
            }
        }
    }
    public void destroy(){
        bt.stopService();
    }

    public boolean isConnected() {
        return bt.getServiceState() == BluetoothState.STATE_CONNECTED ? true : false;
    }


    public BluetoothSPP getBluetoothManager (Activity activity) {
        if(bt == null)
            bt = new BluetoothSPP(activity);
        return bt;
    }

    private void showSnackBar(String msg, Activity activity) {
        LinearLayout view = (LinearLayout) activity.findViewById(R.id.layoutMain);
        Snackbar.make(view, msg, Snackbar.LENGTH_SHORT).show();
    }

}
